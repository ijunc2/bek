package com.bf.main.framework.internal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OutputManagement {
	private static class OutputManagementHolder{
		public static final OutputManagement instance = new OutputManagement();
	}
	
	private OutputManagement() {}
	
	public static OutputManagement getInstance () {
		return OutputManagementHolder.instance;
	}
	
	public Output getOutput(HttpServletRequest req, HttpServletResponse res) {
		// TODO Auto-generated method stub
		return new Output(req, res);
	}
	
}