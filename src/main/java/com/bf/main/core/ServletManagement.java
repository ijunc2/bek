package com.bf.main.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.bf.main.framework.iActive;
import com.bf.main.framework.iRequest;
import com.bf.main.framework.iResponse;
@SuppressWarnings("rawtypes")
public class ServletManagement {
	private static class ServletManagementHolder {
		public static final ServletManagement instance = new ServletManagement();
	}
	private ServletManagement() {}
	
	public static ServletManagement getInstance() {
		return ServletManagementHolder.instance;
	}
	
	// instances
	protected static Map<String, Object> actionsMap; 		// 
	
	// functions
	private Map loadActions(Configuration config) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		BeanManagement bm = BeanManagement.getInstance();
		Map<String, Object> tmp = new HashMap<>();
		
		Iterator iter = config.getKeys();
		while(iter.hasNext()) {
			String str = (String)iter.next();
			tmp.put(str, bm.load(config.getString(str)));
		}
		return tmp;
	}
	public boolean initialize() throws ConfigurationException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Configuration config = new PropertiesConfiguration("actions.properties");
		this.actionsMap = this.loadActions(config);
		return true;
	}
	
	public iResponse activeEngine(iRequest request) {
		iActive active = getAction(request);
		iResponse result ;
		try {
			result = active.execute(request);
		}catch(Exception e) {
			result = active.handleException(request, e);
		}
		return result;
	}
	
	private iActive getAction(iRequest req) {
		return (iActive)this.actionsMap.get(req.getPath());
				
	}
	
	
	
	public boolean destroy() {
		return true;
	}
}
