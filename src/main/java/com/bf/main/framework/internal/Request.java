package com.bf.main.framework.internal;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bf.main.framework.iRequest;

public class Request implements iRequest{
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	
	public Request(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse) {
		// TODO Auto-generated constructor stub
		this.request = paramHttpServletRequest;
	    this.response = paramHttpServletResponse;
	}
	
	@Override
	public String getPath() {
		// TODO Auto-generated method stub
		return request.getAttribute("javax.servlet.include.servlet_path") != null ? (String)request.getAttribute("javax.servlet.include.servlet_path") : request.getServletPath();
	}

	@Override
	public Object getAdapter(Class<HttpServletRequest> class1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStringParams(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map getMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map getMapPlain() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getClientIp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDomain() {
		// TODO Auto-generated method stub
		return null;
	}

}
