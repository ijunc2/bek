package com.bf.main.framework.internal;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bf.main.framework.iOutput;

public class Output implements iOutput{
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected String outputEncoding;
	
	public Output(HttpServletRequest request, HttpServletResponse response){
		this.request = request;
	    this.response = response;
	    this.outputEncoding = "utf-8";
	}
	
	@Override
	public void write(final String paramString) throws Exception {
		// TODO Auto-generated method stub
		PrintWriter out = null;
	    this.response.resetBuffer();
	    this.response.setContentType(getContentType());
	    out = this.response.getWriter();
	    out.print(paramString);
 	}
	
	protected String getContentType(){
		if(this.outputEncoding == null){
			this.outputEncoding = "utf-8";
		}
	    return "application/json; charset=" + this.outputEncoding;
	}
}
