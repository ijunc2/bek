package com.bf.main.core;

public class BeanManagement {
	private static class BeanManagementHolder {
		public static final BeanManagement instance = new BeanManagement();
	}
	private BeanManagement() {}
	
	public static BeanManagement getInstance() {
		return BeanManagementHolder.instance;
	}
	
	public Object load(String param) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		return Class.forName(param).newInstance();
	}
}
