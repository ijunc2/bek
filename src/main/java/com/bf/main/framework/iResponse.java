package com.bf.main.framework;

import com.bf.main.framework.internal.Output;

public interface iResponse {
	public abstract void response(Output iOutput) throws Exception;
}
