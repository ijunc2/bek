package com.bf.main.framework.internal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bf.main.framework.iRequest;

public class RequestManagement {
	private static class RequestManagementHolder{
		public static final RequestManagement instance = new RequestManagement();
	}
	
	private RequestManagement() {}
	
	public static RequestManagement getInstance() {
		return RequestManagementHolder.instance;
	}
	
	public iRequest getRequest(
			HttpServletRequest req,
			HttpServletResponse res) {
		// TODO Auto-generated method stub
	    return new Request(req, res);
	}
}
