package com.bf.main.framework.internal;

import com.bf.main.framework.iResponse;

public class ResponseManagement {
	private static class ReponseManagementHolder {
		public static final ResponseManagement instance = new ResponseManagement();
	}
	
	private ResponseManagement() {}
	
	public static ResponseManagement getInstance() {
		return ReponseManagementHolder.instance;
	}
	
	public iResponse write(final String paramString) {
		return new iResponse() {
			@Override
			public void response(Output output) throws Exception {
				// TODO Auto-generated method stub
				output.write(paramString);
			}
		};
	}
}
 