package com.bf.main.framework.internal;

import java.util.Map;

import com.bf.main.framework.iActive;
import com.bf.main.framework.iRequest;
import com.bf.main.framework.iResponse;

public abstract class Active extends WorkSpace implements iActive{
	
	public abstract iResponse run(iRequest res);
	
	@Override
	public iResponse execute(iRequest req) throws Exception {
		// TODO Auto-generated method stub
		iResponse res;
		
		try {
			res = run(req);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		
		return res;
	}

	@Override
	public iResponse handleException(iRequest request, Exception ex) {
		// TODO Auto-generated method stub
		return write("error");
	}
}
