package com.bf.main.framework;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface iRequest {
	public abstract String getPath();

	public abstract Object getAdapter(Class<HttpServletRequest> class1);

	public abstract String getStringParams(String string);
	
	public Map getMap();
	
	public Map getMapPlain();
	
	public String getClientIp();
	
	public String getDomain();
}
