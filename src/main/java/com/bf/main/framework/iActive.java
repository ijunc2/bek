package com.bf.main.framework;

import java.util.Map;

import com.bf.main.framework.iRequest;
import com.bf.main.framework.iResponse;

public interface iActive {
	public abstract iResponse execute(iRequest req) throws Exception;
	
	public abstract iResponse handleException(iRequest req, Exception ex);
}
